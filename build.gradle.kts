import java.io.ByteArrayOutputStream
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    kotlin("jvm") version "1.3.11"
}

group = "it.unibo.alchemist"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://dl.bintray.com/alchemist-simulator/Alchemist/")
    maven("https://dl.bintray.com/protelis/Protelis/")
}

dependencies {
    implementation("com.google.guava:guava:26.0-jre")
//    api("it.unibo.alchemist:alchemist:8.0.0-beta+008.ed0ca")
//    compile("it.unibo.alchemist:alchemist:8.0.0-beta+008.ed0ca")
    implementation("it.unibo.alchemist:alchemist:8.0.0-beta+008.ed0ca")
    implementation("it.unibo.alchemist:alchemist-maps:8.0.0-beta+008.ed0ca")
    implementation("it.unibo.alchemist:alchemist-implementationbase:8.0.0-beta+008.ed0ca")
    implementation("it.unibo.alchemist:alchemist-loading:8.0.0-beta+008.ed0ca")
    implementation("it.unibo.alchemist:alchemist-time:8.0.0-beta+008.ed0ca")
    implementation("org.apache.commons:commons-math3:3.6.1")
    implementation("org.danilopianini:jirf:0.1.4")
    implementation("org.danilopianini:thread-inheritable-resource-loader:0.3.0")
    implementation("org.mapsforge:mapsforge-map:0.6.1")
    implementation("org.slf4j:slf4j-api:1.8.0-beta2")
    implementation("org.yaml:snakeyaml:1.23")
    implementation("org.protelis:protelis-interpreter:11.0.0-beta+0oe.49d6")
    implementation(kotlin("stdlib-jdk8"))
}

configurations {
    configurations.all {
        resolutionStrategy {
            failOnVersionConflict()
            dependencySubstitution {
                substitute(module("com.google.code.findbugs:findbugs")).with(module("com.github.spotbugs:spotbugs:3.1.10"))
            }
            exclude(group = "com.google.code.findbugs")
            force(
                "com.google.guava:guava:26.0-jre",
                "commons-codec:commons-codec:1.11",
                "commons-io:commons-io:2.6",
                "commons-logging:commons-logging:1.1.1",
                "org.apache.xmlgraphics:xmlgraphics-commons:2.3",
                "org.danilopianini:boilerplate:0.1.8",
                "org.eclipse.emf:org.eclipse.emf.common:2.15.0",
                "org.eclipse.emf:org.eclipse.emf.ecore:2.15.0",
                "org.eclipse.xtend:org.eclipse.xtend.lib:2.16.0",
                "org.jetbrains:annotations:16.0.3",
                "org.jetbrains.kotlin:kotlin-stdlib:1.3.11",
                "org.scala-lang:scala-library:2.12.2",
                "org.scala-lang:scala-reflect:2.12.2",
                "org.slf4j:slf4j-api:1.8.0-beta2",
                "xml-apis:xml-apis:2.0.2"
            )
        }
    }
    implementation {
        println("$this can be resolved: $isCanBeResolved")
        setCanBeResolved(true)
        println("$this can be resolved: $isCanBeResolved")
        resolve()
    }
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

sourceSets.getByName("main") {
    resources {
        srcDirs("src/main/protelis")
    }
}

task("runTests") {
    doLast {
        println("Done.")
    }
}
fun makeTest(
    file: String,
    name: String = file,
    effects: String? = null,
    sampling: Double = 1.0,
    time: Double = Double.POSITIVE_INFINITY,
    vars: Set<String> = setOf(),
    maxHeap: Long? = null,
    taskSize: Int = 1024,
    threads: Int? = null,
    debug: Boolean = false
) {
    val heap: Long = maxHeap ?: if (System.getProperty("os.name").toLowerCase().contains("linux")) {
        ByteArrayOutputStream().use { output ->
                exec {
                    executable = "bash"
                    args = listOf("-c", "cat /proc/meminfo | grep MemAvailable | grep -o '[0-9]*'")
                    standardOutput = output
                }
                output.toString().trim().toLong() / 1024
            }
            .also { println("Detected ${it}MB RAM available.") }  * 9 / 10
    } else {
        // Guess 16GB RAM of which 2 used by the OS
        14 * 1024L
    }
    val threadCount = threads ?: maxOf(1, minOf(Runtime.getRuntime().availableProcessors(), heap.toInt() / taskSize ))
    println("Running on $threadCount threads")
    task<JavaExec>("$name") {
        classpath = sourceSets["main"].runtimeClasspath
        classpath("src/main/protelis")
        main = "it.unibo.alchemist.Alchemist"
        maxHeapSize = "${heap}m"
        if (debug) {
            jvmArgs("-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=1044")
        }
        File("data").mkdirs()
        args(
            "-y", "src/main/resources/${file}.yml",
            "-t", "$time",
            "-p", threadCount,
            "-i", "$sampling"
        )
        if (effects != null) {
            args("-g", "effects/${effects}")
        }
        if (vars.isNotEmpty()) {
            args("-b", "-var", *vars.toTypedArray())
            args("-e", "data/${name}")
            tasks {
                "runTests" {
                    dependsOn("$name")
                }
            }
        }
    }
}
makeTest("test-sgcg", name = "launchGUI", effects = "test.aes")
makeTest("test-sgcg", time = 1200.0, vars = setOf("seed", "shutdownProbability", "backoffAlpha", "peoplecount"))
defaultTasks("runTests")