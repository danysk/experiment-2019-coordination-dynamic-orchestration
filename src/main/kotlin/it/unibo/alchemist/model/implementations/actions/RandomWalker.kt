package it.unibo.alchemist.model.implementations.actions
import it.unibo.alchemist.model.implementations.molecules.SimpleMolecule
import it.unibo.alchemist.model.implementations.times.DoubleTime
import it.unibo.alchemist.model.interfaces.GeoPosition
import it.unibo.alchemist.model.interfaces.MapEnvironment
import it.unibo.alchemist.model.interfaces.Node
import it.unibo.alchemist.model.interfaces.Reaction
import it.unibo.alchemist.model.interfaces.Time
import org.apache.commons.math3.random.RandomGenerator

open class RandomWalker @JvmOverloads constructor(
    protected val randomEngine: RandomGenerator,
    environment: MapEnvironment<GeoPosition>,
    node: Node<GeoPosition>,
    // TODO: make reaction accessible in superclass
    protected val reaction: Reaction<GeoPosition>,
    protected val minimumPosition: GeoPosition,
    protected val maximumPosition: GeoPosition,
    speed: Double = TargetWalker.DEFAULT_SPEED,
    trackMoleculeName: String = "waypoint",
    protected val spaceTolerance: Double = 30.0,
    protected val timeTolerance: Time = DoubleTime(3600.0)
) : TargetWalker<GeoPosition>(environment, node, reaction, trackMoleculeName, speed) {
    private var next: GeoPosition? = null
    private var lastTimeChanged: Time = DoubleTime.ZERO_TIME
    private val waypointName = SimpleMolecule(trackMoleculeName)

    @JvmOverloads constructor(
        randomEngine: RandomGenerator,
        environment: MapEnvironment<GeoPosition>,
        node: Node<GeoPosition>,
        reaction: Reaction<GeoPosition>,
        minimumPosition: Collection<out Number>,
        maximumPosition: Collection<out Number>,
        speed: Double = TargetWalker.DEFAULT_SPEED,
        trackMolecule: String = "waypoint",
        spaceTolerance: Double = 30.0,
        timeTolerance: Time = DoubleTime(600.0)
    ) : this (randomEngine, environment, node, reaction,
        environment.makePosition(*minimumPosition.toTypedArray()),
        environment.makePosition(*maximumPosition.toTypedArray()),
        speed, trackMolecule, spaceTolerance, timeTolerance
    )

    protected fun computeNext(): GeoPosition = (maximumPosition - minimumPosition).let {
        environment.makePosition(
            minimumPosition.latitude + it.latitude * randomEngine.nextDouble(),
            minimumPosition.longitude + it.longitude * randomEngine.nextDouble())
    }
    override fun execute() {
        val myPosition by lazy { environment.getPosition(node) }
        next = next ?.takeIf { myPosition.getDistanceTo(it) > spaceTolerance }
            ?.takeIf { reaction.tau - lastTimeChanged < timeTolerance }
            ?: computeNext().also { lastTimeChanged = reaction.tau }
        node.setConcentration(waypointName, next)
        super.execute()
    }
}
