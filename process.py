import numpy as np
import xarray as xr
import re

def distance(val, ref):
    return abs(ref - val)
vectDistance = np.vectorize(distance)

def getClosest(sortedMatrix, column, val):
    while len(sortedMatrix) > 3:
        half = int(len(sortedMatrix) / 2)
        sortedMatrix = sortedMatrix[-half - 1:] if sortedMatrix[half, column] < val else sortedMatrix[: half + 1]
    if len(sortedMatrix) == 1:
        result = sortedMatrix[0].copy()
        result[column] = val
        return result
    else:
        safecopy = sortedMatrix.copy()
        safecopy[:, column] = vectDistance(safecopy[:, column], val)
        minidx = np.argmin(safecopy[:, column])
        safecopy = safecopy[minidx, :].A1
        safecopy[column] = val
        return safecopy

def convert(column, samples, matrix):
    return np.matrix([getClosest(matrix, column, t) for t in samples])

def valueOrEmptySet(k, d):
    return (d[k] if isinstance(d[k], set) else {d[k]}) if k in d else set()

def mergeDicts(d1, d2):
    """
    Creates a new dictionary whose keys are the union of the keys of two
    dictionaries, and whose values are the union of values.

    Parameters
    ----------
    d1: dict
        dictionary whose values are sets
    d2: dict
        dictionary whose values are sets

    Returns
    -------
    dict
        A dict whose keys are the union of the keys of two dictionaries,
    and whose values are the union of values

    """
    res = {}
    for k in d1.keys() | d2.keys():
        res[k] = valueOrEmptySet(k, d1) | valueOrEmptySet(k, d2)
    return res

def extractCoordinates(filename):
    """
    Scans the header of an Alchemist file in search of the variables.

    Parameters
    ----------
    filename : str
        path to the target file
    mergewith : dict
        a dictionary whose dimensions will be merged with the returned one

    Returns
    -------
    dict
        A dictionary whose keys are strings (coordinate name) and values are
        lists (set of variable values)

    """
    with open(filename, 'r') as file:
        regex = re.compile(' (?P<varName>[a-zA-Z]+) = (?P<varValue>[-+]?\d*\.?\d+(?:[eE][-+]?\d+)?),?')
        dataBegin = re.compile('\d')
        for line in file:
            match = regex.findall(line)
            if match:
                return {var : float(value) for var, value in match}
            elif dataBegin.match(line[0]):
                return {}

def extractVariableNames(filename):
    """
    Gets the variable names from the Alchemist data files header.

    Parameters
    ----------
    filename : str
        path to the target file

    Returns
    -------
    list of list
        A matrix with the values of the csv file

    """
    with open(filename, 'r') as file:
        dataBegin = re.compile('\d')
        lastHeaderLine = ''
        for line in file:
            if dataBegin.match(line[0]):
                break
            else:
                lastHeaderLine = line
        if lastHeaderLine:
            regex = re.compile(' (?P<varName>\S+)')
            return regex.findall(lastHeaderLine)
        return []

def openCsv(path):
    """
    Converts an Alchemist export file into a list of lists representing the matrix of values.

    Parameters
    ----------
    path : str
        path to the target file

    Returns
    -------
    list of list
        A matrix with the values of the csv file

    """
    regex = re.compile('\d')
    with open(path, 'r') as file:
        lines = filter(lambda x: regex.match(x[0]), file.readlines())
        return [[float(x) for x in line.split()] for line in lines]

if __name__ == '__main__':
    # CONFIGURE SCRIPT
    directory = 'data'
    pickleOutput = 'data_summary'
    experiments = ['test-sgcg']
    floatPrecision = '{: 0.2f}'
    seedVars = ['seed']
    timeSamples = 500
    minTime = 0
    maxTime = 1200
    timeColumnName = 'time'
    logarithmicTime = False
    
    # Setup libraries
    np.set_printoptions(formatter={'float': floatPrecision.format})
    # Read the last time the data was processed, reprocess only if new data exists, otherwise just load
    import pickle
    import os
    newestFileTime = max(os.path.getmtime(directory + '/' + file) for file in os.listdir(directory))
    try:
        lastTimeProcessed = pickle.load(open('timeprocessed', 'rb'))
    except:
        lastTimeProcessed = -1
    shouldRecompute = newestFileTime != lastTimeProcessed
    if not shouldRecompute:
        try:
            means = pickle.load(open(pickleOutput + '_mean', 'rb'))
            stdevs = pickle.load(open(pickleOutput + '_std', 'rb'))
        except:
            shouldRecompute = True
    if shouldRecompute:
        timefun = np.logspace if logarithmicTime else np.linspace
        means = {}
        stdevs = {}
        for experiment in experiments:
            # Collect all files for the experiment of interest
            import fnmatch
            allfiles = filter(lambda file: fnmatch.fnmatch(file, experiment + '_*.txt'), os.listdir(directory))
            allfiles = [directory + '/' + name for name in allfiles]
            allfiles.sort()
            # From the file name, extract the independent variables
            dimensions = {}
            for file in allfiles:
                dimensions = mergeDicts(dimensions, extractCoordinates(file))
            dimensions = {k: sorted(v) for k, v in dimensions.items()}
            # Add time to the independent variables
            dimensions[timeColumnName] = range(0, timeSamples)
            # Compute the matrix shape
            shape = tuple(len(v) for k, v in dimensions.items())
            # Prepare the Dataset
            dataset = xr.Dataset()
            for k, v in dimensions.items():
                dataset.coords[k] = v
            varNames = extractVariableNames(allfiles[0])
            for v in varNames:
                if v != timeColumnName:
                    novals = np.ndarray(shape)
                    novals.fill(float('nan'))
                    dataset[v] = (dimensions.keys(), novals)
            # Compute maximum and minimum time, create the resample
            timeColumn = varNames.index(timeColumnName)
            allData = { file: np.matrix(openCsv(file)) for file in allfiles }
            computeMin = minTime is None
            computeMax = maxTime is None
            if computeMax:
                maxTime = float('-inf')
                for data in allData.values():
                    maxTime = max(maxTime, data[-1, timeColumn])
            if computeMin:
                minTime = float('inf')
                for data in allData.values():
                    minTime = min(minTime, data[0, timeColumn])
            timeline = timefun(minTime, maxTime, timeSamples)
            # Resample
            for file in allData:
                allData[file] = convert(timeColumn, timeline, allData[file])
            # Populate the dataset
            for file, data in allData.items():
                dataset[timeColumnName] = timeline
                for idx, v in enumerate(varNames):
                    if v != timeColumnName:
                        darray = dataset[v]
                        experimentVars = extractCoordinates(file)
                        darray.loc[experimentVars] = data[:, idx].A1
            # Fold the dataset along the seed variables, producing the mean and stdev datasets
            means[experiment] = dataset.mean(seedVars)
            stdevs[experiment] = dataset.std(seedVars)
        # Save the datasets
        pickle.dump(means, open(pickleOutput + '_mean', 'wb'), protocol=-1)
        pickle.dump(stdevs, open(pickleOutput + '_std', 'wb'), protocol=-1)
        pickle.dump(newestFileTime, open('timeprocessed', 'wb'))

    # Prepare the charting system
    import matplotlib
    import matplotlib.pyplot as plt
    import matplotlib.cm as cmx
    figure_size=(6, 4)
    matplotlib.rcParams.update({'axes.titlesize': 14})
    matplotlib.rcParams.update({'axes.labelsize': 13})
#    colormap = cmx.gist_rainbow
    colormap = cmx.viridis
    
    # One line per each backoff value, time on x, 500 people, shutdown 0.5
    # One line per coordinate for each coordinate, average of the rest, time on x, one chart per data
    data = means['test-sgcg']
    varMeanings = {
        "edge[Sum]" : "∑ of edge servers",
        "disabled[Sum]" : "∑ of disabled edge servers",
        "isLeader[Sum]" : "∑ of elected edge servers",
        "sgcg[Mean]" : "E of clients per edge server",
        "sgcg[StandardDeviation]" : "σ of clients per edge server",
        "sgcg[Sum]" : "∑ of clients served",
        "sourceLevel[Mean]" : "E of feedback adjustment (m)",
        "sourceLevel[StandardDeviation]" : "σ of feedback adjustment (m)",
    }
    for var in data.data_vars:
        chartsetdata = data[var]
        xdata = chartsetdata['time'] / 60
        reifiedcoords = [x for x in chartsetdata.coords if x != 'time']
        for coord in reifiedcoords:
            fig = plt.figure(figsize = figure_size)
            ax = fig.add_subplot(1, 1, 1)
            ax.set_xlabel("Simulated time (minutes)")
            ax.set_xlim(min(xdata), max(xdata))
            ax.set_title(coord)
            ax.set_ylabel(varMeanings[chartsetdata.name])
#            ax.set_yscale('log')
            mergedata = chartsetdata.mean([x for x in reifiedcoords if x != coord])
            for linename in mergedata[coord]:
                chartdata = mergedata.loc[{coord: linename.values}]
                ax.plot(xdata, chartdata, label = str(linename.values))
            ax.legend()
    
    # Prepare selected charts
    # Evaluation of the backoff parameter
    def makechart(title, ylabel, ydata, colors = None):
        fig = plt.figure(figsize = figure_size)
        ax = fig.add_subplot(1, 1, 1)
        ax.set_title(title)
        xdata = means['test-sgcg']['time'] / 60
        ax.set_xlabel("Simulated time (minutes)")
        ax.set_ylabel(ylabel)
#        ax.set_ylim(0)
        ax.set_xlim(min(xdata), max(xdata))
        plt.axvline(x=10, color='black', linestyle='dashed')
        index = 0
        for (label, data) in ydata.items():
            ax.plot(xdata, data, label=label, color=colors(index / (len(ydata) - 1)) if colors else None)
            index += 1
        return (fig, ax)
    
    chartdata = data['sourceLevel[Mean]'].mean(['shutdownProbability', 'peoplecount'])
    (fig, ax) = makechart(
        "Evaluation of the feedback loop for varying α",
        varMeanings['sourceLevel[Mean]'],
        {
            "disabled": chartdata[0],
            "α=0.001": chartdata[2],
            "α=0.01": chartdata[3],
            "α=0.1": chartdata[4],
            "α=1": chartdata[5],
            "α=0": chartdata[1],
        },
#        colors = cmx.jet
    )
    ax.legend(ncol = 2)
    upper_ylim = 135
    ax.set_ylim(0, upper_ylim)
    fig.savefig('feedbackalpha0.pdf')

    chartdata = data['sourceLevel[StandardDeviation]'].mean(['shutdownProbability', 'peoplecount'])
    (fig, ax) = makechart(
        "Evaluation of the feedback loop for varying α",
        varMeanings['sourceLevel[StandardDeviation]'],
        {
            "disabled": chartdata[0],
            "α=0.001": chartdata[2],
            "α=0.01": chartdata[3],
            "α=0.1": chartdata[4],
            "α=1": chartdata[5],
            "α=0": chartdata[1],
        }
    )
    ax.set_ylim(0, upper_ylim)
    ax.legend(ncol = 2, loc='best', bbox_to_anchor=(0.5, 0., 0.5, 0.5))
    fig.savefig('feedbackalpha1.pdf')
    mixcolormap = lambda x: cmx.winter(1 - x * 2) if x < 0.5 else cmx.YlOrRd((x - 0.5) * 2 * 0.7 + 0.3) # cmx.winter(1 - (x - 0.5) * 2)
    from string import Template
    for aggregator in ['Mean', 'StandardDeviation', 'Sum']:
        varname = 'sgcg[' + aggregator + ']'
        nofeedback = data[varname].sel(shutdownProbability=1, backoffAlpha=-1)
        feedback = data[varname].sel(shutdownProbability=1, backoffAlpha=0.01)
        template = Template('${status}-${users}')
        chartentries = {
            template.substitute(status = status, users = str(int(users))) : 
                (feedback if (status == 'on') else nofeedback).sel(peoplecount=users) 
            for status in ['on', 'off'] for users in feedback['peoplecount'] }
        (fig, ax) = makechart(
            "System performance with active users (ρ=1)",
            varMeanings[varname],
            chartentries,
            colors = mixcolormap
        )
        ax.set_yscale('log')
        ax.set_ylim(ax.get_ylim()[0] * 0.2)
        def sort_labels_by_regex(regex, group_name, ax):
            def labelRegexSorter(plot):
                (_, label) = plot
                match = re.match(regex, label)
                return float(match.group(group_name))
            handles, labels = ax.get_legend_handles_labels()
            hl = sorted(zip(handles, labels), key=labelRegexSorter)
            handles2, labels2 = zip(*hl)
            ax.legend(handles2, labels2, ncol=2)
        usercount = 'o(?:n|ff) (?P<count>\d*) u'
#        sort_labels_by_regex(usercount, 'count', ax)
        ax.legend(ncol=2, handletextpad=0.2, columnspacing=0.2)
        fig.savefig(aggregator + "Performance.pdf")

        chartdata = data[varname].sel(peoplecount=500)
        template = Template('${status}-ρ=${rho}')
        chartentries = {
            template.substitute(status = status, rho = str(rho)) : 
                chartdata.sel(shutdownProbability=rho, backoffAlpha= 0.01 if status == 'on' else -1)
            for status in ['on', 'off'] for rho in chartdata['shutdownProbability'].values
        }
        (fig, ax) = makechart(
            "System resilience to disruption (500 users)",
            varMeanings[varname],
            chartentries,
            colors = mixcolormap
        )
        ax.set_ylim(ax.get_ylim()[0] * 0.2)
#        sort_labels_by_regex('fb o(?:n|ff) .=(?P<count>\d*.\d*)', 'count', ax)
        ax.legend(ncol=2, handletextpad=0.2, columnspacing=0.2, loc='best', bbox_to_anchor=(0., 0., 1, 0.5))
        fig.savefig(aggregator+"Resilience.pdf")
        
        
        